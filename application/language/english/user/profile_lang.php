<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

$lang['dashboard']                    = 'Dashboard';
$lang['profile']                      = "Profile";
$lang['list_profile_description']     = "List of Profile with details";
$lang['no_profile']                   = "No Profile Available to View";
$lang['list_profile']                 = "Profile List";
$lang['add_profile']                  = "Add Profile";
$lang['view_profile']                 = "View Profile";
$lang['view_profile_description']     = "View Profile details";
$lang['edit_profile']                 = "Edit Profile";
$lang['form_username']                = "Username";
$lang['form_email']                   = "Email";
$lang['form_phonenumber']             = "Phone Number";
$lang['form_address']                 = "Address";
$lang['form_distid']                  = "District";
$lang['form_blockid']                 = "Block";
$lang['form_kssp_unit']               = "KSSP Unit";
$lang['form_lsgiid']                  = "LSGI Name";
$lang['form_profile']                 = "Profile Information";
$lang['form_sumbitprofile']           = "Create Profile";
$lang['form_editprofile']             = "Save Profile";
$lang['profile_successfully_created'] = "Profile is successfully created";
$lang['profile_successfully_edited']  = "Profile is successfully edited";
$lang['profile_error']                = "Error in creating the Profile";
