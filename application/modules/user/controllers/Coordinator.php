<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Coordinator extends CI_Controller {
	// Protected or private properties
	protected $_template;

	public function __construct() {
		parent::__construct();
		//TODO :Set admin check here from aauth
$this->load->language('user/coordinator', 'malayalam');

		$this->load->model('user/Coordinator_model', 'coordinator');
		$this->access_library->check_coordinator();
		}

	public function dashboard() {
        
		$data['page_data'] = array(
			'title'       => "Keralapadanam 2.0",
			'description' => "Keralapadanam 2.0 coordinator Dashboard",
		);
              if (!isset($this->session->basicinfoid))
               { 
			$this->session->set_userdata('basicinfoid', '0');
               }
               elseif ($this->session->basicinfoid == '0')
               {
		    $this->session->set_userdata('basicinfoid', '0');
                }
                else
                {
            redirect('user/coordinator/houses/'.$this->session->basicinfoid);
                }
                
		$this->_template['page'] = 'coordinator/coordinator';
		$this->system_library->load_user($this->_template['page'], $data);
	}
    
    public function houses_list($basicinfo = '0') {

        if ($this->aauth->is_loggedin()) {
            $data['page_data'] = array('title' => "Keralapadanam 2.0 coordinator Dashboard",
				'description'                      => "Keralapadanam 2.0 coordinator Dashboard.",
			);
			$userid           = $this->aauth->get_user()->id;
			$data['username'] = $this->aauth->get_user()->username;
			$distid    = $this->coordinator->get_district_by_userid($userid);
			$data['homes']    = $this->coordinator->get_houses_by_distid($distid['distid']);

                          if (!isset($this->session->basicinfoid))
               { 
			$this->session->set_userdata('basicinfoid', '0');
               }
               elseif ($this->session->basicinfoid == '0')
               {
		    $this->session->set_userdata('basicinfoid', '0');
                }
                else
                {
            redirect('user/coordinator/houses/'.$this->session->basicinfoid);
                }

            $this->_template['page'] = 'coordinator/dashboard';
            $this->system_library->load_user($this->_template['page'], $data, false);
        } else {
            redirect('/user/login');
        }

    }
        public function houses($basicinfoid = '0'){ //should add assigned house filter aka submitted from useralloc
            
            if ($this->aauth->is_loggedin()) {
			$userid    = $this->aauth->get_user()->id;
   			$distid    = $this->coordinator->get_district_by_userid($userid);
			if ($this->coordinator->get_houses_by_distid($distid['distid'], $basicinfoid) === 'FALSE') {
        $this->session->set_userdata('basicinfoid', 0);
				redirect('coordinator/dashboard');
			} else {
        $basicinfo = $this->coordinator->get_userstate_by_basicinfoid($basicinfoid);
          $this->session->set_userdata('basicinfoid', $basicinfo['id']);
					redirect('survey/forms/');
				
			}
		} else {
			redirect('/user/login');
		}

            
        }

    public function status() {
        $data['homeslider'] = true;
        $data['distid'] = $this->coordinator->get_distid_drop();
        $data['page_data'] = array(
            'title' => "Get LSG Data",
            );
        $this->_template['page'] = 'coordinator/status/select';
        $this->system_library->load_user($this->_template['page'], $data);
    }

    public function getstats() {
        $distid            = $this->input->post('distid');
        if($distid == 'all') {
            $district      = array('district'=>'14');
            $houses        = $this->coordinator->get_all_houses();
        } else {
            $district      = $this->coordinator->get_dist_by_id($distid);
            //$lsgicount   = $this->status->get_lsgi_count($distid);
            //$housecount  = $this->status->get_house_count($distid);
            $houses        = $this->coordinator->get_houses_by_dist($distid);
        }
        $status            = array('draft'=>0,'wip'=>0,'working'=>0,'submitted'=>0,'invalid'=>0);
        $uar               = array();
        $lsgi              = array();
        $mekh              = array();
        $stats             = array();
        $submitted         = 0;
        $inprogress        = 0;
        $i                 = 0;
        $prev              = NULL;
        $data['lsgicount'] = NULL;
        $data['mekhcount'] = NULL;
        //vdebug($houses);
        if($distid == 'all') {
            foreach($status as $sk=>$sv) {
                foreach ($houses as $h) {
                    if($h['status'] == $sk) {
                        if($sk == 'submitted') { $submitted++;  }
                        if($sk == 'working')   { $inprogress++; }
                        $uar[$h['district']][$sk][$i] = $h;
                        $mekh[$i] = $h['mekhala'];
                        $lsgi[$i] = $h['lsgi'];
                        if($h['district'] != $prev) { $sv=0; }
                        $stats[$h['district']][$sk] = ++$sv;
                        $prev = $h['district'];
                        $i++;
                    }
                }
            }
            $mekhcount          = count(array_unique($mekh));
            $data['mekhcount']  = $mekhcount;
        } else {
          foreach($status as $sk=>$sv) {
            foreach ($houses as $h) {
              if($h['status'] == $sk) {
                if($sk == 'submitted') { $submitted++;  }
                if($sk == 'working')   { $inprogress++; }
                $uar[$h['mekhala']][$sk][$i] = $h;
                $lsgi[$i] = $h['lsgi'];
                if($h['mekhala'] != $prev) { $sv=0; }
                $stats[$h['mekhala']][$sk] = ++$sv;
                $prev = $h['mekhala'];
                $i++;
              }
            }
          }
        }
        $lsgicount          = count(array_unique($lsgi));
        $data['lsgicount']  = $lsgicount;
        $data['uar']        = $uar;
        $data['submitted']  = $submitted;
        $data['inprogress'] = $inprogress;
        $data['district']   = $district['district'];
        $data['housecount'] = $i;
        $data['stats']      = $stats;
        $data['page_data']  = array(
            'title'         => "Keralapadanam Survey Status",
            'description'   => "Keralapadanam Survey Status by District",
        );
        $this->_template['page'] = 'coordinator/status/summary';
        $this->system_library->load_user($this->_template['page'], $data);
    }

    }



