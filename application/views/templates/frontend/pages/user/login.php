<div class="content-wrapper">
<div class="row">
<div class="col-md-12">
<div class="login-box">
  <div class="login-logo">
    <b>User</b> Login
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <?php if (validation_errors()) :?>
    <div class="callout callout-error"  style="color:#C82626">
              <h4>Error!</h4>
              <p><?php echo validation_errors('<span class="error">', '</span>'); ?></p>
            </div>
    <?php endif;?>
    <?php if (isset($error) and !empty($error)) : ?>
         <div class="callout callout-error"  style="color:#C82626">
              <h4>Error!</h4>
              <p><?= $error; ?></p>
            </div>
    <?php endif; ?>
    <?php
    $message = $this->session->flashdata('f_message');
    $type = $this->session->flashdata('f_type');
    if ($message) :
    ?>
   <div class="callout callout-<?= $type; ?>"  style="color:#C82626">
        <p><?= $message; ?></p>
      </div>
    <?php endif; ?>
    <?php echo form_open('user/login', 'class="email" id="myform"'); ?>
      <div class="form-group has-feedback">
        <?php echo form_input(array ('name'=>'authEmail', 'id'=>'email', 'class'=>'form-control', 'type'=>'email','placeholder'=>'Email', 'value'=>'', 'required' => 'required'));?>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <?php echo form_password(array ('name'=>'authPassword', 'id'=>'password', 'class'=>'form-control', 'type'=>'password','placeholder'=>'Password', 'value'=>'', 'required' => 'required'));?>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember">
                <?php echo form_checkbox('remember', 'accept', false); ?> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    <?php echo form_close();?>
    <hr/>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</div>
</div>
</div>