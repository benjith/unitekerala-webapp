
<br/>
<h1>About Kerala Padanam</h1>
<p>
<img src="<?php echo base_url();?>static/img/Keralapadanam2.jpg" alt="Kerala Padanam 2" />
</p>
<p><strong>A Kerala Study - How does Kerala Live? How does Kerala think?</strong></p>
<p><a href="http://kssp.in">Kerala Sasthra Sahitya Parishad</a> is conducting a survey of the living conditions in Kerala. The survey is titled <strong>"Keralapadanam " a.k.a "Kerala Study - How does Kerala Live? How does Kerala think?"</strong>.</p><p> The survey will cover about 6000 households across Kerala where the activists will spend hours with families to understand their lifestyle and thoughts on various subjects.</p>
<p>The survey is designed in such a way that a group of people interviews another group of people in the form of an evening tea discussion or debate. The interviewers are carefully selected and trained them to avoid any kind of bias in the data. Nearly 6000 volunteers are participating in this survey.</p> 
<p>
<a class="btn btn-flat btn-primary" href="<?php echo base_url();?>video">Kerala Padanam Videos</a>
</p>

<h2>Kerala Padanam 1</h2>
<p>In 2004, KSSP conducted the first edition of Kerala Padanam. It covered about 6000 households in the state. The majour findings of the survey given as. The population of Kerala has been divided into four classes or groups. Around 40% of the people are very poor, and only a minority of about 10% of the people belong to the upper middle class and control their state of affairs. More and more people are moving towards extreme poverty and the rich minority are growing their wealth. The findings have been published as Kerala Padanam Report</p>

<h2>Download Kerala Padanam 1 Report</h2>
<p>The report of survey conducted in 2004 is now available for download. Click the link below to dowload the report</p>

<p><a href="http://wiki.kssp.in/w/images/a/ab/Kerala_padanam_final.pdf" class="btn btn-flat btn-primary" target="_blank">Download Kerala Padanam Report Malayalam</a></p>

<h2>Targets of Kerala Padanam 2</h2>
<p>The main targets of Kerala Padanam 2 are given below</p>
<ul class="homeul">
<li>Collect data to analize the socio economic conditions of people in Kerala</li>
<li>Analyze how financial status, religion, caste, work status affect the quality of life in Kerala</li>
<li>Give creative suggessions and project to rebuild a better Kerala based on this data</li>
<li>Find the areas which require more deep and detailed study</li>
</ul>
