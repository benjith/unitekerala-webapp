<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller
{
    // Protected or private properties
    protected $_template;

    public function __construct() 
    {
        parent::__construct();
        $this->load->language('user/status', 'malayalam');

        $this->load->model('user/Status_model', 'status');
        $this->access_library->check_user();
    }
    public function index()
    { 
      // 4:TARGET`>

	    $data['page_data'] = array(
			  'title'       => "Keralapadanam 2.0 Status",
			  'description' => "Keralapadanam 2.0 User Status",
      );
      $data['mainstat']= $this->status->getmainstatus();
      $this->_template['page'] = 'user/status';
      $this->system_library->load_user($this->_template['page'], $data, false);
    }
}
