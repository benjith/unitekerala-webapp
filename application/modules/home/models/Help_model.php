<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Help_model extends CI_Model {
	// Protected or private properties
	protected $_table;

	// Constructor
	public function __construct() {
		parent::__construct();
		$this->_table = $this->config->item('database_tables');
	}

  public function get_helptext_by_field($fieldname) {
    $this->db->select('id, helptext');
    $this->db->where('fieldname', $fieldname);
    $query = $this->db->get($this->_table['help'], 1);
    if ($query->num_rows() == 1) {
      return $query->row_array();
    }
  }
}
