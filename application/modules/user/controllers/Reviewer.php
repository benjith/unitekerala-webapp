<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reviewer extends CI_Controller {
	// Protected or private properties
	protected $_template;

	public function __construct() {
		parent::__construct();
		//TODO :Set admin check here from aauth
$this->load->language('user/reviewer', 'malayalam');

		$this->load->model('user/Reviewer_model', 'reviewer');
		$this->access_library->check_reviewer();
		}

	public function dashboard() {
		$data['page_data'] = array(
			'title'       => "Keralapadanam 2.0",
			'description' => "Keralapadanam 2.0 Reviewer Dashboard",
		);
        
              if (!isset($this->session->basicinfoid))
               { 
			$this->session->set_userdata('basicinfoid', '0');
               }
               elseif ($this->session->basicinfoid == '0')
               {
		    $this->session->set_userdata('basicinfoid', '0');
                }
                else
                {
            redirect('user/reviewer/houses/'.$this->session->basicinfoid);
                }
        
		$this->_template['page'] = 'reviewer/reviewer';
		$this->system_library->load_user($this->_template['page'], $data);
	}
    
    public function houses_list($basicinfo = '0') {
        if ($this->aauth->is_loggedin()) {
            $data['page_data'] = array('title' => "Keralapadanam 2.0 Reviewer Dashboard",
				'description'                      => "Keralapadanam 2.0 Reviewer Dashboard.",
			);
			$userid           = $this->aauth->get_user()->id;
			$data['username'] = $this->aauth->get_user()->username;
			$distid    = $this->reviewer->get_district_by_userid($userid);
			$data['homes']    = $this->reviewer->get_houses_by_distid($distid['distid']);

                          if (!isset($this->session->basicinfoid))
               { 
			$this->session->set_userdata('basicinfoid', '0');
               }
               elseif ($this->session->basicinfoid == '0')
               {
		    $this->session->set_userdata('basicinfoid', '0');
                }
                else
                {
            redirect('user/reviewer/houses/'.$this->session->basicinfoid);
                }

            $this->_template['page'] = 'reviewer/dashboard';
            $this->system_library->load_user($this->_template['page'], $data, false);
        } else {
            redirect('/user/login');
        }

    }
        public function houses($basicinfoid = '0'){ //should add assigned house filter aka submitted from useralloc
            
            if ($this->aauth->is_loggedin()) {
			$userid    = $this->aauth->get_user()->id;
   			$distid    = $this->reviewer->get_district_by_userid($userid);
			if ($this->reviewer->get_houses_by_distid($distid['distid'], $basicinfoid) === 'FALSE') {
        $this->session->set_userdata('basicinfoid', 0);
				redirect('reviewer/dashboard');
			} else {
        $basicinfo = $this->reviewer->get_userstate_by_basicinfoid($basicinfoid);
          $this->session->set_userdata('basicinfoid', $basicinfo['id']);
					redirect('survey/forms/');
				
			}
		} else {
			redirect('/user/login');
		}

            
        }

    public function status() {
        $userid            = $this->aauth->get_user_id();
        $dist              = $this->reviewer->get_distid($userid);
        $distid            = $dist['distid'];
        $district          = $this->reviewer->get_dist_by_id($distid);
        //$lsgicount       = $this->status->get_lsgi_count($distid);
        //$housecount      = $this->status->get_house_count($distid);
        $houses            = $this->reviewer->get_houses_by_dist($distid);
        $status            = array('draft'=>0,'wip'=>0,'working'=>0,'submitted'=>0,'invalid'=>0);
        $uar               = array();
        $lsgi              = array();
        $stats             = array();
        $submitted         = 0;
        $inprogress        = 0;
        $i                 = 0;
        $prev              = NULL;

        foreach($status as $sk=>$sv) {
            foreach ($houses as $h) {
              if($h['status'] == $sk) {
                if($sk == 'submitted') { $submitted++;  }
                if($sk == 'working')   { $inprogress++; }
                $uar[$h['mekhala']][$sk][$i] = $h;
                $lsgi[$i] = $h['lsgi'];
                if($h['mekhala'] != $prev) { $sv=0; }
                $stats[$h['mekhala']][$sk] = ++$sv;
                $prev = $h['mekhala'];
                $i++;
              }
            }
        }
        //vdebug($stats);
        $lsgicount          = count(array_unique($lsgi));
        $data['lsgicount']  = $lsgicount;
        $data['uar']        = $uar;
        $data['submitted']  = $submitted;
        $data['inprogress'] = $inprogress;
        $data['district']   = $district['dist_en'];
        $data['housecount'] = $i;
        $data['stats']      = $stats;
        $data['page_data']  = array(
            'title'         => "Keralapadanam Survey Status",
            'description'   => "Keralapadanam Survey Status by District",
        );
        $this->_template['page'] = 'reviewer/status/summary';
        $this->system_library->load_user($this->_template['page'], $data);
    }

    }


