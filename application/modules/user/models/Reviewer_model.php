<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Reviewer_model extends CI_Model {
	public function __construct() {
		parent::__construct();

		$this->_table = $this->config->item('database_tables');
        
        
	}
    
    public function get_district_by_userid($userid){
        $this->db->select('distid');
        $this->db->from($this->_table['profile']);
        $this->db->where('id',$userid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
			$result = $query->row_array();
			return $result;
		}
        
        
        
    }
    	public function get_houses_by_distid($distid) {
		$this->db->select('basicinfo.id,housenumber,housename,lsgiid,ward');
		$this->db->from($this->_table['basicinfo']);
		$this->db->select('lsgi.lsgi as lsgi');
		$this->db->join($this->_table['lsgi'], 'lsgiid = lsgi.id');
        $this->db->join($this->_table['useralloc'], 'basicinfo.id = useralloc.basicinfoid');
        if ($this->access_library->is_districtuser()){
            $this->db->where('useralloc.status','working');
            $this->db->where('distid', $distid);
            $this->db->or_where('useralloc.status','submitted');
            $this->db->where('distid', $distid);
        }
        else
        {
            $this->db->where('useralloc.status','submitted');
            $this->db->where('distid', $distid);
        }
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = $query->result_array();
			return $result;
		} else {
			return FALSE;
		}
	}
    
    
    public function get_house_by_userid($userid, $basicinfoid) {
		$this->db->select('distid,id');
		$this->db->from($this->_table['basicinfo']);
		$this->db->where('distid', $distid);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = $query->row_array();
			return TRUE;
		} else {
			return FALSE;
		}
	}
    
    
    	public function get_userstate_by_basicinfoid($basicinfoid) {

		$this->db->select('s_basicinfo.id');
		$this->db->from($this->_table['basicinfo'] . ' s_basicinfo');
		$this->db->where('id', $basicinfoid);
		$this->db->limit(1);

		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			$result = $query->row_array();
			return $result;
		}
	}



    public function change_status($form_data, $basicinfoid) {
      $this->db->where('basicinfoid', $basicinfoid);
      $this->db->update($this->_table['useralloc'],$form_data);


      if ($this->db->affected_rows() == '1') {
          return true;
      }

      return false;
    }


    public function get_distid($userid) {
        $this->db->select('distid');
        $this->db->where('id', $userid);
        $query = $this->db->get($this->_table['profile']);
        if($query->num_rows() == 1) {
            return $query->row_array();
        }
    }

    public function get_dist_by_id($distid) {
        $this->db->select('district as distname,dist_en,distcode as district');
        $this->db->from($this->_table['district']);
        $this->db->where('id', $distid);
        $query = $this->db->get();
        if($query->num_rows() == 1) {
            return $query->row_array();
        }
    }

    public function get_houses_by_dist($distid=1) {
        $this->db->select('bi.id as basicinfoid');
        $this->db->from($this->_table['basicinfo']. ' bi');
        $this->db->select('l.lsgi,l.mekhala');
        $this->db->join($this->_table['lsgi'] . ' l', 'bi.lsgiid = l.id', 'left');
        $this->db->select('ua.status');
        $this->db->join($this->_table['useralloc'] . ' ua', 'bi.id = ua.basicinfoid', 'left');
        //$this->db->group_by('bi.lsgiid');
        $this->db->where('bi.distid',  $distid);
        $this->db->order_by('l.mekhala');
        $query = $this->db->get();
        $res =array();
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
        }
        return $res;
    }



}
