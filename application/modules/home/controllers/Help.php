<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Help extends CI_Controller {
	// Protected or private properties
	// protected $_template;

	public function __construct() {
		parent::__construct();
		$this->load->model('home/help_model','help');
		// $this->load->language('admin/help', 'english');
	}

  public function tip($fieldname='0') {
    $helpdata = $this->help->get_helptext_by_field($fieldname);
    echo '<button type="button" class="close" aria-hidden="true">×</button>'.$helpdata['helptext'];
  }
}
