<?php defined('BASEPATH') OR exit('No direct script access allowed');

// General
$lang['dashboard']						       = "Dashboard";
$lang['home']						             = "Homepage";
$lang['home_description'] 		       = "Here you can manage Homepage settings";

// $lang['create_item']					     = "Create a home field";
// $lang['create_item_description']  = "Here you can create a new home item.";

$lang['edit']						             = "Edit";

// Messages
$lang['no_items']						         = "No home fields found";
// $lang['delete_confirmation']			 = "Are you sure you want to delete this home field?";
// $lang['successfully_created']		 = "Home field has been successfully created";
// $lang['successfully_deleted']	   = "Home field has been successfully deleted";
$lang['home_edited']	               = "Homepage has been successfully edited";
$lang['home_error']				           = "Homepage has not been updated, please try again!";

// Table header
$lang['th_name']					           = "Field";
$lang['th_value']						         = "Value";

$lang['title']                       = 'Title';
$lang['description']                 = 'Description';
$lang['url']                         = 'URL';
$lang['url_text']                    = 'URL Title';
$lang['icon']                        = 'Icon';
$lang['content']                     = 'Content';

/* End of file home_lang.php */
/* Location: ./application/modules/admin/language/english/home_lang.php */
