<?php if (!defined('BASEPATH')) {
exit('No direct script access allowed');
}

    class Data_model extends CI_Model {
        public function __construct() {
            parent::__construct();

            $this->_table = $this->config->item('database_tables');
        }

        public function get_distid_drop() {
            $this->db->select('id,district');
            $query = $this->db->get($this->_table['district']);

            if ($query->num_rows() > 0) {
                $data['select'] = "Select District";
                foreach ($query->result_array() as $row) {
                    $data[$row['id']] = $row['district'];
                }
                return $data;
            } else {
                $nodata      = array();
                $nodata['0'] = array('-2' => 'No Data Selected');
                return $nodata;
            }
        }

      public function get_mekhala_by_dist($distid=1) {
        $this->db->select('id,mekhalaname');
        $this->db->from('mekhala');
        $this->db->where('distid', $distid);
        $this->db->order_by("mekhalaname", "asc");
        $query = $this->db->get();
        return $query->result();
      }

      public function get_mekhala_by_dist_ar($distid=1) {
        $this->db->select('id as lsgicode,mekhalaname');
        $this->db->from('mekhala');
        $this->db->where('distid ', $distid);
        $this->db->order_by("mekhalaname", "asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          //$data['select'] = "Select";
          //$data['all'] = "All";
          foreach ($query->result_array() as $row) {
            $data[$row['lsgicode']] = $row['mekhalaname'];
          }
          return $data;
        } else {
          $nodata      = array();
          $nodata['0'] = array('-2' => 'No Data Selected');
          return $nodata;
        }
      }

        public function get_lsgi_by_district($distid=1,$typeid='G') {
            $this->db->select('lsgicode,lsgi');
            $this->db->from('lsgi');
            $this->db->where('districtid ', $distid);
            $this->db->order_by("lsgi", "asc");
            $this->db->like('lsgicode', $typeid,'after');
            $query = $this->db->get();
            return $query->result();
        }

        public function get_lsgi_by_district_ar($distid=1,$typeid='G') {
            $this->db->select('lsgicode,lsgi');
            $this->db->from('lsgi');
            $this->db->where('districtid ', $distid);
            $this->db->order_by("lsgi", "asc");
            $this->db->like('lsgicode', $typeid,'after');
            $query = $this->db->get();
             if ($query->num_rows() > 0) {
                //$data['select'] = "Select";
                $data['all'] = "All";
                foreach ($query->result_array() as $row) {
                    $data[$row['lsgicode']] = $row['lsgi'];
                }
                return $data;
            } else {
                $nodata      = array();
                $nodata['0'] = array('-2' => 'No Data Selected');
                return $nodata;
            }
        }

        public function get_ward_by_lsgi($lsgiid='G14023',$typeid='M') {
            $this->db->select('id,District,pancode,Name,GP_Ward,wardcode,polcode,polstr,slno,pname,gname,houseno,housename,genage,idcard');
        /*
        if($typeid=='M'){
            $this->db->from('finalmuns');
        } elseif ($typeid=='G'){
            $this->db->from('finalpans');
        } elseif ($typeid=='C'){
            $this->db->from('finalcorps');
        }
        */
            $this->db->from('finaldata');
            $this->db->where('pancode ', $lsgiid);
            //$this->db->group_by('houseno');
            $this->db->order_by("GP_Ward", "asc");
            $this->db->like('pancode', $typeid,'after');
            $query = $this->db->get();
            $data=$query->result_array();
            if ($query->num_rows() > 0) {
                return $data;
            } else {
                $nodata[0]['id']='0';
                $nodata[0]['pancode']='0';
                $nodata[0]['Name']='NO Data Available';
                return $nodata;
            }
        }

        public function get_ward_by_mekhala($mekhalaid=1) {
          $this->db->select('s_finaldata.id,s_finaldata.District,s_finaldata.pancode,s_finaldata.Name,s_finaldata.GP_Ward,s_finaldata.wardcode,s_finaldata.polcode,s_finaldata.polstr,s_finaldata.slno,s_finaldata.pname,s_finaldata.gname,s_finaldata.houseno,s_finaldata.housename,s_finaldata.genage,s_finaldata.idcard');
        /*
        if($typeid=='M'){
            $this->db->from('finalmuns');
        } elseif ($typeid=='G'){
            $this->db->from('finalpans');
        } elseif ($typeid=='C'){
            $this->db->from('finalcorps');
        }
        */
          $this->db->from('finaldata s_finaldata');
          $this->db->join('(SELECT lsgicode FROM `lsgi` WHERE mekhalaid = ' . $mekhalaid . ') as a2', 's_finaldata.pancode = a2.lsgicode');
            $this->db->order_by("s_finaldata.pancode", "asc");
            $query = $this->db->get();
            $data=$query->result_array();
            if ($query->num_rows() > 0) {
                return $data;
            } else {
                $nodata[0]['id']='0';
                $nodata[0]['pancode']='0';
                $nodata[0]['Name']='NO Data Available';
                return $nodata;
            }
        }

        public function get_ward_by_dist($distid='Kasaragod',$typeid='M') {
            $this->db->select('id,District,pancode,Name,GP_Ward,wardcode,polcode,polstr,slno,pname,gname,houseno,housename,genage,idcard');
            /*
              if($typeid=='M'){
              $this->db->from('finalmuns');
              } elseif ($typeid=='G'){
              $this->db->from('finalpans');
              } elseif ($typeid=='C'){
              $this->db->from('finalcorps');
              }
            */
            $this->db->from('finaldata');
            $this->db->where('distid', $distid);
            //$this->db->group_by('polcode, houseno');
            $this->db->order_by("Name,GP_Ward,housename", "asc");
            $this->db->like('pancode', $typeid,'after');
            $query = $this->db->get();
            $data=$query->result_array();
            if ($query->num_rows() > 0) {
                return $data;
            } else {
                $nodata[0]['id']='0';
                $nodata[0]['pancode']='0';
                $nodata[0]['Name']='NO Data Available';
                return $nodata;
            }        
        }

        public function get_all_lsgis() {
            $this->db->select('lsgicode,lsgi,distid');
            $this->db->from('lsgi');
            $this->db->like('lsgicode', 'G', 'after');
            $this->db->order_by("lsgi", "asc");
            $query = $this->db->get();
            $data = $query->result_array();
            if ($query->num_rows() > 0) {
                return $data;
            }
        }

        public function get_district_by_id($distid) {
            $this->db->select('district');
            $this->db->from('district');
            $this->db->where('id',$distid);
            $this->db->limit(1);
            $query = $this->db->get();
            if ($query->num_rows() == 1) {
                return $query->row_array();
            }
        }

        public function get_wards($lsgiid) {
            $this->db->select('wardcode');
            $this->db->from('finalpans');
            $this->db->where('pancode',$lsgiid);
            $query = $this->db->get();
            $data = $query->result_array();
            if ($query->num_rows() > 0) {
                return $data;
            }
        }
    }
