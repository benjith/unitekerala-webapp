<section class="content-header">
<h1><?php echo $this->lang->line('edit_profile');  ?></h1>

<ol class="breadcrumb">
<li><a href="<?php echo base_url();?>admin"><?php echo $this->lang->line('home');
?></a> <span class="divider">/</span></li>
<li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url();?>admin/dashboard"><?php echo $this->lang->line('dashboard');
?></a> <span class="divider">/</span></li>
<li><a href="<?php echo base_url(); ?>user/list_profile"><?php echo $this->lang->line('profile'); ?></a> </li>
<li class="active"><?php echo $this->lang->line('edit_profile');?></li>
</ol>
</section>
<section class="content">
<div class="row"><div class="col-md-12">
<?php if($this->session->flashdata('message')): ?>
  <div class="alert alert-info">
<button type="button" class="close" data-dismiss="alert">×</button>
<strong>Message: </strong><?php echo $this->session->flashdata('message');?>
  </div>
  <?php endif;?>
  <?php if($this->session->flashdata('error')): ?>
  <div class="alert alert-error">
<button type="button" class="close" data-dismiss="alert">×</button>
<strong>Error: </strong>Some Error in submitting the form. Check the required fields.
  </div>
  <?php endif;?>
  <?php if (validation_errors()):?>
  <div class="alert alert-error">
<button type="button" class="close" data-dismiss="alert">×</button>
<strong>Error: </strong>Some Error in submitting the form. Check the required fields.
  </div>
  <?php endif;?>
  <div class="box box-primary">
      <div class="box-header with-border">
      <h3 class="box-title"><?php echo $this->lang->line('form_profile');?></h3>
      </div>
<?php echo form_open('user/profile/editprofile/'.$editingid, array('class' => 'form-horizontal', 'id' => 'myform'), array('profile_id' => $editingid));?>
    <div class="box-body">


<div class="form-group">
<label class="col-sm-2 control-label" for="username"><?php echo  $this->lang->line('form_username'); ?>  <span class="required">*</span></label>
<div class="col-sm-10">
  <input id="username" type="text" name="username" value="<?php echo $username; ?>"  size="35" class="form-control" />
<span class="error"><?php echo form_error('username'); ?></span>
</div></div>


<div class="form-group">
<label class="col-sm-2 control-label" for="phonenumber"><?php echo  $this->lang->line('form_phonenumber'); ?>  <span class="required">*</span></label>
<div class="col-sm-10">
  <input id="phonenumber" type="text" name="phonenumber" value="<?php echo $profile['phonenumber']; ?>"  size="35" class="form-control" />
<span class="error"><?php echo form_error('phonenumber'); ?></span>
</div></div>


<div class="form-group">
<label class="col-sm-2 control-label" for="address"><?php echo  $this->lang->line('form_address'); ?>  <span class="required">*</span></label>
<div class='col-sm-10'>
<?php echo form_error('address'); ?>
<?php echo form_textarea( array( 'class' => 'form-control', 'name' => 'address', 'rows' => '5', 'cols' => '80', 'value' => set_value('address',$profile['address']) ) )?>
</div></div>

<div class="form-group">
<label class="col-sm-2 control-label" for="jilla"><?php echo  $this->lang->line('form_jilla'); ?>  <span class="required">*</span></label>
<div class="col-sm-10">
  <input id="jilla" type="text" name="jilla" value="<?php echo $profile['jilla']; ?>"  size="35" class="form-control" />
<span class="error"><?php echo form_error('jilla'); ?></span>
</div></div>


<div class="form-group">
<label class="col-sm-2 control-label" for="city"><?php echo  $this->lang->line('form_city'); ?>  <span class="required">*</span></label>
<div class="col-sm-10">
  <input id="city" type="text" name="city" value="<?php echo $profile['city']; ?>"  size="35" class="form-control" />
<span class="error"><?php echo form_error('city'); ?></span>
</div></div>


<div class="form-group">
<label class="col-sm-2 control-label" for="kssp_unit"><?php echo  $this->lang->line('form_kssp_unit'); ?>  <span class="required">*</span></label>
<div class="col-sm-10">
  <input id="kssp_unit" type="text" name="kssp_unit" value="<?php echo $profile['kssp_unit']; ?>"  size="35" class="form-control" />
<span class="error"><?php echo form_error('kssp_unit'); ?></span>
</div></div>



</div>
<div class="box-footer">
  <input type="submit" name="submit" value="<?php echo $this->lang->line('form_editprofile'); ?>" class="btn btn-primary" />
</div></div>
<?php echo form_close(); ?>
<hr/>
<div class="box-footer">
<a  class="btn btn-info" href="<?php echo base_url(); ?>user/profile/list_profile"><?php echo $this->lang->line('list_profile'); ?></a>
           |
<a class="btn btn-info" href="<?php echo base_url(); ?>user/profile/"><?php echo $this->lang->line('profile'); ?></a>
           </div>
</div>
</section>
