<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {
	// Protected or private properties
	protected $_table;

	// Constructor
	public function __construct() {
		parent::__construct();

		$this->_table = $this->config->item('database_tables');
	}

	// Public methods
	public function get_home_data() {
		$this->db->select('id,name,value,lang');
		$this->db->from($this->_table['home']);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
	}
  //
	// public function edit_home_data() {
	// 	$data = array(
	// 					  'name' => $this->input->post('name'),
	// 					  'url_name' => url_title($this->input->post('name')),
	// 					  'description' => $this->input->post('description')
	// 				  );
  //
	// 	$this->db->where('id', $this->input->post('id'));
	// 	$this->db->update($this->_table['categories'], $data);
	// }
}

/* End of file categories_model.php */
/* Location: ./application/modules/admin/models/categories_model.php */
