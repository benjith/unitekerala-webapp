<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Access_library {

	protected $_table;

	// Constructor
	public function __construct() {
		if (!isset($this->CI)) {
			$this->CI = &get_instance();
		}

		$this->_table = $this->CI->config->item('database_tables');

	}

	// Public methods
	public function is_logged_in() {
		if ($this->aauth->is_loggedin() == true) {
			return true;
		} else {
			return false;
		}
	}

	public function is_admin() {
		if ($this->CI->aauth->is_admin() == true) {
			return true;
		} else {
			return false;
		}
	}

	public function is_statesec() {
		if ($this->CI->aauth->is_member('Statesec') != true) {
			return false;
		} else {
			return true;
		}
	}

	public function is_statetresh() {
		if ($this->CI->aauth->is_member('Statetresh') != true) {
			return false;
		} else {
			return true;
		}
	}

	public function is_distsec() {
		if ($this->CI->aauth->is_member('Distsec') != true) {
			return false;
		} else {
			return true;
		}
	}

	public function is_disttresh() {
		if ($this->CI->aauth->is_member('Disttresh') != true) {
			return false;
		} else {
			return true;
		}
	}

	public function is_mekhalasec() {
		if ($this->CI->aauth->is_member('Mekhalasec') != true) {
			return false;
		} else {
			return true;
		}
	}

	public function is_mekhalatresh() {
		if ($this->CI->aauth->is_member('Mekhalatresh') != true) {
			return false;
		} else {
			return true;
		}
	}

	public function is_unitsec() {
		if ($this->CI->aauth->is_member('Unitsec') != true) {
			return false;
		} else {
			return true;
		}
	}

	public function is_user() {
		if ($this->CI->aauth->is_member('User') != true) {
			return false;
		} else {
			return true;
		}
	}

	public function check_logged_in() {
		if ($this->CI->aauth->is_loggedin() != true) {
			redirect('user/login', 'refresh');
			exit();
		}
	}

	public function check_access() {
		if ($this->CI->aauth->is_loggedin() == true) {
			if ($this->CI->session->userdata['basicinfoid'] == 0) {
				redirect('user/dashboard', 'refresh');
				exit();
			}
		} else {
			redirect('user/login', 'refresh');
			exit();
		}
	}

	public function update_userstatus($form_data, $userid) {
		$this->CI->db->where('id', $userid);
		$this->CI->db->update($this->_table['profile'], $form_data);
		if ($this->CI->db->affected_rows() == '1') {
			return TRUE;
		}
		return FALSE;
	}

	public function check_state() {
		if ($this->CI->aauth->is_member('Statesec') != true && $this->CI->aauth->is_member('Statetresh') != true) {
			$isloggedin = 0;
			$form_data  = array('is_loggedin' => $isloggedin);
			$userid     = $this->CI->aauth->get_user()->id;
			$this->CI->aauth->logout();
			$this->update_userstatus($form_data, $userid);
			redirect('/user/login');
			exit();
		}
	}
    public function check_statesec() {
		if ($this->CI->aauth->is_member('Statesec') != true) {
			$isloggedin = 0;
			$form_data  = array('is_loggedin' => $isloggedin);
			$userid     = $this->CI->aauth->get_user()->id;
			$this->CI->aauth->logout();
			$this->update_userstatus($form_data, $userid);
			redirect('/user/login');
			exit();
		}
	}
	public function check_statetresh() {
		if ($this->CI->aauth->is_member('Statetresh') != true) {
			$isloggedin = 0;
			$form_data  = array('is_loggedin' => $isloggedin);
			$userid     = $this->CI->aauth->get_user()->id;
			$this->CI->aauth->logout();
			$this->update_userstatus($form_data, $userid);
			redirect('/user/login');
			exit();
		}
	}

	public function check_dist() {
		if ($this->CI->aauth->is_member('Distsec') != true && $this->CI->aauth->is_member('Disttresh') != true) {
			$isloggedin = 0;
			$form_data  = array('is_loggedin' => $isloggedin);
			$userid     = $this->CI->aauth->get_user()->id;
			$this->CI->aauth->logout();
			$this->update_userstatus($form_data, $userid);
			redirect('/user/login');
			exit();
		}
	}public function check_distsec() {
		if ($this->CI->aauth->is_member('Distsec') != true) {
			$isloggedin = 0;
			$form_data  = array('is_loggedin' => $isloggedin);
			$userid     = $this->CI->aauth->get_user()->id;
			$this->CI->aauth->logout();
			$this->update_userstatus($form_data, $userid);
			redirect('/user/login');
			exit();
		}
	}
	public function check_disttresh() {
		if ($this->CI->aauth->is_member('Disttresh') != true) {
			$isloggedin = 0;
			$form_data  = array('is_loggedin' => $isloggedin);
			$userid     = $this->CI->aauth->get_user()->id;
			$this->CI->aauth->logout();
			$this->update_userstatus($form_data, $userid);
			redirect('/user/login');
			exit();
		}
	}

	public function check_mekhala() {
		if ($this->CI->aauth->is_member('Mekhalasec') != true && $this->CI->aauth->is_member('Mekhalatresh') != true) {
			$isloggedin = 0;
			$form_data  = array('is_loggedin' => $isloggedin);
			$userid     = $this->CI->aauth->get_user()->id;
			$this->CI->aauth->logout();
			$this->update_userstatus($form_data, $userid);
			redirect('/user/login');
			exit();
		}
	}public function check_mekhalasec() {
		if ($this->CI->aauth->is_member('Mekhalasec') != true) {
			$isloggedin = 0;
			$form_data  = array('is_loggedin' => $isloggedin);
			$userid     = $this->CI->aauth->get_user()->id;
			$this->CI->aauth->logout();
			$this->update_userstatus($form_data, $userid);
			redirect('/user/login');
			exit();
		}
	}
	public function check_mekhalatresh() {
		if ($this->CI->aauth->is_member('Mekhalatresh') != true) {
			$isloggedin = 0;
			$form_data  = array('is_loggedin' => $isloggedin);
			$userid     = $this->CI->aauth->get_user()->id;
			$this->CI->aauth->logout();
			$this->update_userstatus($form_data, $userid);
			redirect('/user/login');
			exit();
		}
	}
	
	public function check_unitsec() {
		if ($this->CI->aauth->is_member('Unitsec') != true) {
			$isloggedin = 0;
			$form_data  = array('is_loggedin' => $isloggedin);
			$userid     = $this->CI->aauth->get_user()->id;
			$this->CI->aauth->logout();
			$this->update_userstatus($form_data, $userid);
			redirect('/user/login');
			exit();
		}
	}

	public function check_admin() {
		if ($this->CI->aauth->is_member('Admin') != true) {
			$isloggedin = 0;
			$form_data  = array('is_loggedin' => $isloggedin);
			$userid     = $this->CI->aauth->get_user()->id;
			$this->CI->aauth->logout();
			$this->update_userstatus($form_data, $userid);
			redirect('/user/login');
			exit();
		}
	}
}
/* End of file Access_library.php */
/* Location: ./application/libraries/Access_library.php */
