<section class="content-header">
  <h1><?php echo $this->lang->line('view_profile');  ?></h1>

  <ol class="breadcrumb">
    <li><a href="<?php echo base_url();?>user"><?php echo $this->lang->line('home');?>
    </a> <span class="divider">/</span></li>
    <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url();?>user/dashboard"><?php echo $this->lang->line('dashboard');?>
    </a> <span class="divider">/</span></li>
    <li><a href="<?php echo base_url(); ?>user/list_profile"><?php echo $this->lang->line('list_profile'); ?></a> </li>
    <li class="active">
      <?php echo $this->lang->line('view_profile');?>
    </li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title"><?php echo $this->lang->line('view_profile_description');?></h3>
        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped">

            <tr>
              <td class="col-sm-2">
                <?php echo $this->lang->line('form_username'); ?>
                </span>
                <td class="col-sm-10">
                  <?php echo $username; ?>
                  </span>
            </tr>

            <tr>
              <td class="col-sm-2">
                <?php echo $this->lang->line('form_phonenumber'); ?>
                </span>
                <td class="col-sm-10">
                  <?php echo $profile['phonenumber']; ?>
                  </span>
            </tr>

            <tr>
              <td class="col-sm-2">
                <?php echo $this->lang->line('form_address'); ?>
                </span>
                <td class="col-sm-10">
                  <?php echo $profile['address']; ?>
                  </span>
            </tr>

            <tr>
              <td class="col-sm-2">
                <?php echo $this->lang->line('form_jilla'); ?>
                </span>
                <td class="col-sm-10">
                  <?php echo $profile['jilla']; ?>
                  </span>
            </tr>

            <tr>
              <td class="col-sm-2">
                <?php echo $this->lang->line('form_city'); ?>
                </span>
                <td class="col-sm-10">
                  <?php echo $profile['city']; ?>
                  </span>
            </tr>


            <tr>
              <td class="col-sm-2">
                <?php echo $this->lang->line('form_kssp_unit'); ?>
                </span>
                <td class="col-sm-10">
                  <?php echo $profile['kssp_unit']; ?>
                  </span>
            </tr>


          </table>
        </div>
      </div>

      <p class="dataBtm">
        <?php echo anchor(base_url(). 'user/profile/editprofile/' . $profile['id'], '<span class="btn btn-sm btn-primary">'.$this->lang->line('edit_profile').'</span>'); ?>

      </p>
    </div>
  </div>
</section>
