<?php defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller {
	// Protected or private properties
	protected $_template;

	public function __construct() {
		parent::__construct();

		$this->load->language('user/user', 'english');
		$this->load->library("Aauth");
		$this->load->library('form_validation'); //The Form Validation Loader
		$this->load->model('user/user_model', 'user');
	}

	public function index() {
        //echo $this->aauth->hash_password('aswe34--',3); die;
		if ($this->aauth->is_loggedin()) {
        $isloggedin = 1;
        $form_data = array('is_loggedin' => $isloggedin);
        $userid = $this->aauth->get_user()->id;
        $this->user->update_userlogin_status($form_data, $userid);
			redirect('user/dashboard', 'refresh');
		} else {
			$this->aauth->logout();
			redirect('/');
		}
	}

	public function login() {
		 $data['homeslider']      = true;
        $isloggedin = 0;
		if ($this->aauth->is_loggedin()) {
            $isloggedin = 1;
            $form_data = array('is_loggedin' => $isloggedin);
            $userid = $this->aauth->get_user()->id;
            $this->user->update_userlogin_status($form_data, $userid);
			if ($this->aauth->get_user()->username == 'ksspadmin') {
                redirect('admin/data', 'refresh');
            } elseif ($this->aauth->is_member('Admin')) {
				redirect('admin/dashboard', 'refresh');
			} elseif ($this->aauth->is_member('Default')) {
				redirect('user/dashboard', 'refresh');
			} elseif ($this->aauth->is_member('Coordinator')) {
				redirect('user/coordinator/dashboard', 'refresh');
			} elseif ($this->aauth->is_member('Reviewer')) {
				redirect('user/reviewer/dashboard', 'refresh');
			}

		} else {
			$this->form_validation->set_rules('authEmail', 'Email', 'trim|required|valid_email|xss_clean');
			$this->form_validation->set_rules('authPassword', 'Password', 'trim|required');
			$this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

			if ($this->form_validation->run() === true) { // validation passed success
				if ($this->aauth->login($this->input->post('authEmail'), $this->input->post('authPassword'), $this->input->post('remember')) == true) {
                    $isloggedin = 1;
					$form_data = array('is_loggedin' => $isloggedin);
					$userid    = $this->aauth->get_user_id($this->input->post('authEmail'));
					$this->user->update_userlogin_status($form_data, $userid);
					$this->aauth->set_user_var('Email', $this->input->post('authEmail'));

                    if ($this->aauth->get_user()->username == 'ksspadmin') {
                        redirect('admin/data', 'refresh');
					} elseif ($this->aauth->is_member('Admin')) {
						redirect('admin/dashboard', 'refresh');
					} elseif ($this->aauth->is_member('Default')) {
						$data['userid'] = $this->aauth->get_user()->id;
						$ustatus        = $this->user->get_last_login($data['userid']);
						if ($ustatus['distid'] == '0' || $ustatus['kssp_mekhalaid'] == '0') {
							redirect('user/profile/editprofile/' . $data['userid'], 'refresh');
						} else {
							redirect('user/dashboard', 'refresh');
						}
					} elseif ($this->aauth->is_member('Coordinator')) {
						redirect('user/coordinator/dashboard', 'refresh');
					} elseif ($this->aauth->is_member('Reviewer')) {
						redirect('user/reviewer/dashboard', 'refresh');
					}
				} else {
					$this->aauth->info('User Email or Password does not validated');
					$data['error'] = $this->lang->line('user_login_error');
					// die;
				}
			} else {
				//echo validation_errors();
			}
			$data['page_data'] = array(
				'title'       => "Keralapadanam 2.0 User Login",
				'description' => "Keralapadanam 2.0 User Login Here",
			);
			$this->_template['page'] = 'user/login';
			$this->system_library->load($this->_template['page'], $data);
		}
	}

	public function logout() {
    $isloggedin = 0;
    $form_data = array('is_loggedin' => $isloggedin);
    $userid = $this->aauth->get_user()->id;
		$this->aauth->logout();
    $this->user->update_userlogin_status($form_data, $userid);
		redirect('/');
	}

	public function dashboard() {
		if ($this->aauth->is_loggedin()) {
            if ($this->aauth->is_member('Admin')) {
              redirect('admin/dashboard','refresh');
            }

			$data['page_data'] = array('title' => "Keralapadanam 2.0 User Dashboard",
				'description'                      => "Keralapadanam 2.0 User Dashboard.",
			);
			$userid               = $this->aauth->get_user()->id;
			$data['username']     = $this->aauth->get_user()->username;
			$data['homes']        = $this->user->get_houses_by_userid($userid);
            $data['lsgi']         = $this->user->get_lsgi_by_userid($userid);

			if (!isset($this->session->basicinfoid)) {
				$this->session->set_userdata('basicinfoid', '0');
			} elseif ($this->session->basicinfoid == '0') {
				$this->session->set_userdata('basicinfoid', '0');
			} else {
				redirect('user/houses/' . $this->session->basicinfoid);
			}

			$this->_template['page'] = 'user/dashboard';
			$this->system_library->load_user($this->_template['page'], $data, false);
		} else {
			redirect('/user/login');
		}

	}

	public function changepass() {
		$this->load->library('form_validation'); //The Form Validation Loader
		$this->form_validation->set_rules('password', 'lang:form_password', 'required|trim|xss_clean|max_length[15]');
		if ($this->form_validation->run($this) === true) {
			$a = $this->aauth->update_user($this->aauth->get_user()->id, $this->aauth->get_user()->email, $this->input->post('password'), $this->aauth->get_user()->username);
			if ($a) {
				$this->session->set_flashdata('message', $this->lang->line('user_successfully_edited'));
				redirect('user/dashboard', 'refresh');
				//redirect('myform/success');   // or whatever logic needs to occur
			} else {
				$this->session->set_flashdata('message', $this->lang->line('user_error'));
				redirect('user/changepass', 'refresh');
			}
		}
		$data['id']              = $this->aauth->get_user()->id;
		$this->_template['page'] = 'user/changepass';
		$this->system_library->load_user($this->_template['page'], $data, false);
	}

    public function verify($basicinfoid = '0') {
        $form_data['status'] = 'wip';
        if($this->user->update_status($form_data, $basicinfoid)) {
          $this->session->set_flashdata('message', $this->lang->line('verification_success'));            
        } else {
          $this->session->set_flashdata('error', $this->lang->line('verification_error'));            
        }
        redirect('user/dashboard','refresh');
    }

    public function report($basicinfoid = '0') {
        $form_data['status'] = 'invalid';
        if($this->user->update_status($form_data, $basicinfoid)) {
          $this->session->set_flashdata('message', $this->lang->line('verification_success'));            
        } else {
          $this->session->set_flashdata('error', $this->lang->line('verification_error'));            
        }
        redirect('user/dashboard','refresh');
    }

	public function houses($basicinfoid = '0') {
		if ($this->aauth->is_loggedin()) {
			$userid = $this->aauth->get_user()->id;
			if ($this->user->get_house_by_userid($userid, $basicinfoid) === 'FALSE') {
				$this->session->set_userdata('basicinfoid', 0);
				redirect('user/dashboard');
			} else {
				$basicinfo = $this->user->get_userstate_by_basicinfoid($basicinfoid);
				if ($basicinfo['userid'] === '0') {
					$this->session->set_userdata('basicinfoid', $basicinfo['id']);
					redirect('survey/modify/');
				} elseif ($basicinfo['userid'] === $userid) {
					$this->session->set_userdata('basicinfoid', $basicinfo['id']);
					redirect('survey/forms/');
				} else {
					$this->session->set_userdata('basicinfoid', 0);
					redirect('user/dashboard');
				}
			}
		} else {
			redirect('/user/login');
		}

	}
}

/* End of file user.php */
/* Location: ./application/modules/user/controllers/user.php */
