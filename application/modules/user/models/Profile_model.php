<?php  if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Profile_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->_table = $this->config->item('database_tables');
    }

    /* --------------------------------------------------------------------
        *
    * Get all profile
    * Join Queries are not buided - Do That Manually
    */

    public function get_profile_count()
    {
        $query = $this->db->count_all($this->_table['profile']);
        return $query;
    }

    public function get_profile($number = 10, $offset = 0)
    {
        $this->db->select('s_profile.id,s_profile.username,s_profile.phonenumber,s_profile.address,s_profile.distid,s_profile.lsgiid');
        $this->db->from($this->_table['profile'] . ' s_profile');



        $this->db->limit($number, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            return $result;
        }
    }
    public function get_profile_by_id($id)
    {
        $this->db->select('s_profile.id,s_profile.username,s_profile.phonenumber,s_profile.kssp_mekhalaid,s_profile.address,s_profile.distid,s_profile.kssp_unit,s_profile.lsgiid');

        $this->db->from($this->_table['profile'] . ' s_profile');
        $this->db->where('s_profile.id', $id);

        $this->db->select('0_district.district as districtdistrict');
        $this->db->join($this->_table['district'] . ' 0_district', 's_profile.distid = 0_district.id', 'left');

        $this->db->select('1_lsgi.lsgi as lsgilsgi');
        $this->db->join($this->_table['lsgi'] . ' 1_lsgi', 's_profile.lsgiid = 1_lsgi.id', 'left');

        //$this->db->select('2_block.block as blockblock');
        //$this->db->join($this->_table['block'] . ' 2_block', 's_profile.blockid = 2_block.id', 'left');

        $this->db->select('3_mekhala.mekhalaname as mekhalamekhala');
        $this->db->join($this->_table['mekhala'] . ' 3_mekhala', 's_profile.kssp_mekhalaid = 3_mekhala.id', 'left');

        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row_array();
        }
    }


    public function get_blockid_drop()
    {
        $this->db->select('id,block');
        $this->db->order_by("block", "asc");

        $query = $this->db->get($this->_table['block']);

        if ($query->num_rows() > 0) {
            $data['select'] = "Select Block";

            foreach ($query->result_array() as $row) {
                $data[$row['id']] = $row['block'];
            }

            return $data;
        } else {
            $nodata=array();
            $nodata['0'] = array( '-2' => 'No Data Selected' );
            return $nodata;
        }
    }


    public function get_distid_drop()
    {
        $this->db->select('id,district');
        $this->db->order_by("district", "asc");

        $query = $this->db->get($this->_table['district']);

        if ($query->num_rows() > 0) {
            $data['select'] = "Select District";

            foreach ($query->result_array() as $row) {
                $data[$row['id']] = $row['district'];
            }

            return $data;
        } else {
            $nodata=array();
            $nodata['0'] = array( '-2' => 'No Data Selected' );
            return $nodata;
        }
    }



    public function get_lsgiid_drop()
    {
        $this->db->select('id,lsgi');
        $this->db->order_by("lsgi", "asc");

        $query = $this->db->get($this->_table['lsgi']);

        if ($query->num_rows() > 0) {
            $data['select'] = "Select Lsgi";

            foreach ($query->result_array() as $row) {
                $data[$row['id']] = $row['lsgi'];
            }

            return $data;
        } else {
            $nodata=array();
            $nodata['0'] = array( '-2' => 'No Data Selected' );
            return $nodata;
        }
    }

    public function get_kssp_mekhalaid_drop()
    {
        $this->db->select('id,mekhalaname');
        $this->db->order_by("mekhalaname", "asc");

        $query = $this->db->get($this->_table['mekhala']);

        if ($query->num_rows() > 0) {
            $data['select'] = "Select mekhalaname";

            foreach ($query->result_array() as $row) {
                $data[$row['id']] = $row['mekhalaname'];
            }

            return $data;
        } else {
            $nodata=array();
            $nodata['0'] = array( '-2' => 'No Data Selected' );
            return $nodata;
        }
    }

    public function create_profile($form_data)
    {
        $this->db->insert($this->_table['profile'], $form_data);

        if ($this->db->affected_rows() == '1') {
            return true;
        }

        return false;
    }
    public function edit_profile($form_data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update($this->_table['profile'], $form_data);


        if ($this->db->affected_rows() == '1') {
            return true;
        }

        return false;
    }
    public function get_mekhala_by_district($distid) {
		$this->db->select('id,mekhalaname');
		$this->db->from($this->_table['mekhala']);
		$this->db->where('distid', $distid);
		$this->db->order_by("id", "asc");
		$query = $this->db->get();
		return $query->result();
	}
    public function get_blocks_by_district($distid) {
		$this->db->select('id,block');
		$this->db->from('block');
		$this->db->where('districtid', $distid);
		$this->db->order_by("id", "asc");
		$query = $this->db->get();
		return $query->result();
	}
    public function get_lsgi_by_district($distid) {
		$this->db->select('id,lsgi');
		$this->db->from('lsgi');
		$this->db->where('districtid', $distid);
		$this->db->order_by("lsgi", "asc");
		$query = $this->db->get();
		return $query->result();
	}
    
    public function get_blockid_drop_dist($distid = 1) {
		$this->db->select('id,block');
		$this->db->order_by("block", "asc");
		$this->db->where('districtid', $distid);

		$query = $this->db->get($this->_table['block']);

		if ($query->num_rows() > 0) {
			$data['select'] = "Select Block";

			foreach ($query->result_array() as $row) {
				$data[$row['id']] = $row['block'];
			}

			return $data;
		} else {
			$nodata      = array();
			$nodata['0'] = array('-2' => 'No Data Selected');
			return $nodata;
		}
	}
    
    public function get_lsgiid_drop_dist($distid = 1) {
		$this->db->select('id,lsgi');
		$this->db->order_by("lsgi", "asc");
		$this->db->where('districtid', $distid);

		$query = $this->db->get($this->_table['lsgi']);

		if ($query->num_rows() > 0) {
			$data['select'] = "Select Lsgi";

			foreach ($query->result_array() as $row) {
				$data[$row['id']] = $row['lsgi'];
			}

			return $data;
		} else {
			$nodata      = array();
			$nodata['0'] = array('-2' => 'No Data Selected');
			return $nodata;
		}
	}
    public function get_mekhalaid_drop_dist($distid = 1) {
		$this->db->select('id,mekhalaname');
		$this->db->order_by("mekhalaname", "asc");
		$this->db->where('distid ', $distid);

		$query = $this->db->get($this->_table['mekhala']);

		if ($query->num_rows() > 0) {
			$data['select'] = "Select Mekhala";

			foreach ($query->result_array() as $row) {
				$data[$row['id']] = $row['mekhalaname'];
			}

			return $data;
		} else {
			$nodata      = array();
			$nodata['0'] = array('-2' => 'No Data Selected');
			return $nodata;
		}
	}
}
